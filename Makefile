CC = gcc
CPP = g++
PLUGIN_INCLUDE = -I/usr/lib/gcc/x86_64-linux-gnu/4.8/plugin/include/
CPPFLAGS+= $(PLUGIN_INCLUDE) -fPIC
CFLAGS+= $(PLUGIN_INCLUDE) -fPIC

dumb_plugin.so: dumb_plugin.o
	$(CPP) -shared $^ -o $@
	$(CC) -O -fplugin=./dumb_plugin.so -fplugin-arg-dumb_plugin-ref-pass-name=ccp -fplugin-arg-dumb_plugin-ref-pass-instance-num=1 -c test.c

clean:
	rm -f *o *~ *out *so
